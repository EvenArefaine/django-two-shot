from django.urls import path
from receipts.views import (
    ReceiptListView,
    AccountCreateView,
    ExpenseCategoryCreateView,
    ReceiptCreateView,
    ExpenseCategoryListView,
    AccountListView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="receipt_account_create",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="receipt_category_create",
    ),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="receipt_category_list",
    ),
    path("accounts/", AccountListView.as_view(), name="receipt_account_list"),
]
